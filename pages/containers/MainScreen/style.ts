import styled, { css } from 'styled-components'
const WelcomeUser = styled.div`
  text-align: center;
  font-size: 1rem;
  margin-top: 5rem;
`
export default WelcomeUser
