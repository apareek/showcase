export interface InfoMainScreenProps {
  name: string
}

export interface Education {
  schoolName: string
  degree: string
  stream: string
  startYear: string
  endYear: string
  grade: string
  description: string
}
