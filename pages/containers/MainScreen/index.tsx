import React, { useState, useMemo } from 'react'
import { ModalProvider } from 'styled-react-modal'
import EducationInfoForm from '../../components/Modal'
import Card from '../../components/Card'
import SideCard from '../../components/SideCard'
import WelcomeUser from './style'
import { InfoMainScreenProps, Education } from './mainScreen.interface'

const MainScreen = ({ name }: InfoMainScreenProps) => {
  const [educationList, setEducationList] = useState<Education[]>([])

  const addEducation = (education: Education) => {
    setEducationList([ ...educationList, education ])
  }

  const sortedList = useMemo(
    () => educationList.sort(
      (a, b) => new Date(b.startYear).getTime() - new Date(a.startYear).getTime()
    ),
    [educationList],
  );

  return (
    <>
      <WelcomeUser>
        <h2>Welcome to {name}&apos;s education page</h2>
      </WelcomeUser>
      <ModalProvider>
        <EducationInfoForm addEducation={addEducation} />
      </ModalProvider>
      {sortedList.map((school: Education, index: number) => (
        <Card key={index} school={school} />
      ))}
      {sortedList.length > 0 && <SideCard educationList={sortedList} />}
    </>
  )
}

export default MainScreen
