import type { NextPage } from 'next'
import HomeScreen from './containers/HomeScreen'
import '../styles/Home.module.css'

const Home: NextPage = () => {
  return <HomeScreen />
}

export default Home
