export const inputFields = [
  {
    type: 'text',
    placeholder: 'Degree',
    name: 'degree',
  },
  {
    type: 'text',
    placeholder: 'Field Of Study',
    name: 'stream',
  },
  {
    type: 'text',
    placeholder: 'Grade',
    name: 'grade',
  },
]
  
export const inputDateFields = [
  {
    type: 'date',
    name: 'startYear',
    label: 'Start Date',
    max: new Date().toJSON().slice(0, 10)
  },
  {
    type: 'date',
    name: 'endYear',
    label: 'End Date',
    max: ''
  },
]