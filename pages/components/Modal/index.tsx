import React, { useState } from 'react'
import FormContainer, { StyledModal, Button, Textarea, WrapperDiv } from './style'
import CustomInput from '../CustomInput'
import { handleValidation } from '../../../utils'
import Input from '../Input'
import { EducationInfoFormProps } from './modal.interface'
import DateInput from '../DateInput'
import { StyledSpan } from '../DateInput/style'
import { inputFields, inputDateFields } from './constants'

const EducationInfoForm: React.FC<EducationInfoFormProps> = ({ addEducation }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false)
  const [fields, setFields] = useState<any>({})
  const [errors, setErrors] = useState<any>({})

  const toggleModal = () => {
    setIsOpen(!isOpen)
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLTextAreaElement>) => {
    setFields({ ...fields, [e.target.name]: e.target.value })
  }

  const handleChangeCustomInput = (value: string) => {
    setFields({ ...fields, schoolName: value })
  }

  const formSubmit = () => {
    const isError = handleValidation(fields)
    if (!(Object.keys(isError).length - 1)) {
      const trimedFieldValue = {
        schoolName: fields.schoolName.trim(),
        degree: fields.degree.trim(),
        stream: fields.stream.trim(),
        startYear: fields.startYear.trim(),
        endYear: fields.endYear.trim(),
        grade: fields.grade.trim(),
        description: fields.description.trim(),
      }
      addEducation(trimedFieldValue)
      reset()
      toggleModal()
    } else {
      setErrors(isError)
    }
  }

  const reset = () => {
    setFields({})
    setErrors({})
  }

  return (
    <div>
      <Button onClick={toggleModal}>Add New Education</Button>
      <StyledModal
        isOpen={isOpen}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        beforeClose={reset}
      >
        <FormContainer>
          <fieldset>
            <h3>New Education Modal</h3>
            <WrapperDiv>
            <CustomInput 
              value={fields.schoolName} 
              onChange={handleChangeCustomInput} 
              error={errors["schoolName"]}
            />
            {inputFields.map((field, index) => (
              <Input
                {...field}
                value={fields[field.name]}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => handleChange(e)}
                error={errors[field.name]}
                key={index}
              />
            ))}
            {inputDateFields.map((field, index) => (
              <DateInput
                key={index}
                {...field}
                value={fields[field.name]}
                onChange={handleChange}
                error={errors[field.name]}
              />
            ))}
            </WrapperDiv>
            <Textarea 
              name='description'
              rows={4}
              cols={52}
              placeholder='Not more than 100 words'
              onChange={handleChange}
              value={fields['description']}
            />
            <StyledSpan>{errors['description']}</StyledSpan>
            <Button type='submit' onClick={formSubmit}>
              Save
            </Button>
          </fieldset>
        </FormContainer>
      </StyledModal>
    </div>
  )
}

export default EducationInfoForm
