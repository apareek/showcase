import styled from 'styled-components'
import Modal from 'styled-react-modal'

const FormContainer = styled.div`
  width: 50%;
  height: 60%;
  background-color: white;
  margin: auto;
  padding: 0.7rem;
  margin-top: 12%;
  overflow: auto;
`
export const StyledModal = Modal.styled`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(128, 128, 128, 0.5);
`
export const Button = styled.button`
  display: inline-block;
  color: black;
  font-size: 1em;
  margin: auto;
  padding: 0.5rem;
  border-radius: 3px;
  display: block;
  background-color: rgba(128, 128, 128, 0.5);
`

export const ValueWrapper = styled.input`
  width: 100%;
  padding-left: 8px;
  padding-right: 32px;
  height: 32px;
  box-sizing: border-box;
  border-radius: 1px;
  border: 1px solid #b6c1ce;
  line-height: 32px;
`
export const CustomInput = styled.div`
  position: relative;
  width: 320px;
  `
export const StyledFieldset = styled.fieldset`
  color: black;
`

export const Textarea = styled.textarea`
  width: 96%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 33px auto;
  padding: 10px;
`
export const WrapperDiv = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
`
export default FormContainer
