export interface InfoDateProps {
  label: string
  type: string
  name: string
  value: string
  error: string
  onChange(e: React.ChangeEvent<HTMLInputElement>): void
  max: string
}
