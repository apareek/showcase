import styled, { css } from 'styled-components'

export const StyledDate = styled.input.attrs(() => ({
  size: 50,
}))`
  width:21vw;
  border-radius: 3px;
  border: 1px solid grey;
  display: block;
  margin-top: 15px;
  padding: 20px;
  height:0.01rem;
  width: 68%;

  ::placeholder {
    color: palevioletred;
  }
`

export const StyledSpan = styled.span`
font-size: 14px;
color: red
`

export const WrapperDiv = styled.div`
  display: flex;
  align-items: flex-center 
`
export const StyledLabel = styled.label`
  margin-left: 30px;
  margin-top: 24px;
  width: 17%
`
