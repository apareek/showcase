import React from 'react'
import { StyledSpan } from '../DateInput/style'
import { InfoInputProps } from './input.interface'
import { StyledDiv, StyledInput } from './style'

const Input = (props: InfoInputProps) => {
  return (
    <StyledDiv>
      <StyledInput
        {...props}
      />
      <StyledSpan>{props.error}</StyledSpan>
    </StyledDiv>
  )
}

export default Input
