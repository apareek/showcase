export interface InfoCustomInputProps {
  value: string
  onChange: (text: string) => void
  error: string
}

export interface ISuggestion { 
  name: string 
  code?: string
}