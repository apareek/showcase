import type { NextPage } from 'next'
import MainScreen from './containers/MainScreen'
import { useRouter } from 'next/router'
import '../styles/Home.module.css'
const Main: NextPage = () => {
  const { query } = useRouter()
  return <MainScreen name={query.name} />
}

export default Main
