export const getUniversityList = async (text: string) => {
  const response = await fetch(`http://universities.hipolabs.com/search?name=${text}`)
  const data = await response.json()
  return data
}